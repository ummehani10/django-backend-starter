import os
from corsheaders.defaults import default_methods
from corsheaders.defaults import default_headers

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/2.2/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'l06_cxhblo3d2*b+v7!n@ysskr=e02d=5-(2$b)wy-v-ph4&5c'

# SECURITY WARNING: don't run with debug turned on in production!
exec(f"DEBUG={os.environ.get('DEBUG', 'True')}")

ALLOWED_HOSTS = ['*']

BASE_URL = "obliquitygroup.com"

# Application definition
CUSTOM_APPS = [
    'user',
]

THIRD_PARTY_APPS = [
    'admin_interface',
    # 'flat_responsive', # only if django version < 2.0
    # 'flat', # only if django version < 1.9
    'colorfield',
    # 'django_celery_beat',
    # 'django_seed',
    'django_extensions',
    'rest_framework',
    'markdownx',
]

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'corsheaders',
    *CUSTOM_APPS,
    *THIRD_PARTY_APPS
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'corsheaders.middleware.CorsMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'bugsnag.django.middleware.BugsnagMiddleware',
]

BUGSNAG = {
    'api_key': os.environ.get('BUGSNAG_KEY'),
    'project_root': BASE_DIR,
    "notify_release_stages": ['development', 'production'],
    "release_stage": os.environ.get('BUGSNAG_ENV', 'development'),
}



LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        }
    },
    'formatters': {
        'verbose': {
            'format': ('%(levelname)s %(asctime)s %(module)s'
                       ' %(process)d %(thread)d %(message)s'),
        },
        'simple': {
            'format': '%(levelname)s %(message)s',
        },
    },
    'root': {
        'level': 'INFO',
        'handlers': [
            'bugsnag',
            "console"
        ],
    },
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        },
        'console': {
            'level': 'INFO',
            'class': 'logging.StreamHandler',
            'formatter': 'simple',
        },
        'bugsnag': {
            'level': 'WARNING',
            'class': 'bugsnag.handlers.BugsnagHandler',
        }
    },
    'loggers': {
        'django': {
            'handlers': ['console'],
            'level': 'INFO',
            # 'propagate': False,
        },
        'django.backends': {
            'handlers': ['console'],
            'level': 'INFO',
            # 'propagate': False,
        },
        'django.request': {
            'handlers': ['console'],
            'level': 'INFO',
            # 'propagate': False,
        },
    }
}

for app_name in CUSTOM_APPS:
    LOGGING['loggers'][app_name] = {
        'handlers': ['console'],
        'level': 'INFO',
        # 'propagate': True,
    }

CORS_ORIGIN_ALLOW_ALL = True
CORS_ALLOW_METHODS = list(default_methods)
CORS_ALLOW_HEADERS = list(default_headers) + ['session-token', 'user-id', 'session_token', 'user_id']

ROOT_URLCONF = 'app.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, '')],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'app.wsgi.application'


# Database
# https://docs.djangoproject.com/en/2.2/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': os.environ.get('DATABASE_NAME','obliquity'),
        'USER': os.environ.get('DATABASE_USER','postgres'),
        'PASSWORD': os.environ.get('DATABASE_PASSWORD', 'postgres'),
        'HOST': os.environ.get('DATABASE_HOST'),
        'PORT': '5432',
    }
}


# Password validation
# https://docs.djangoproject.com/en/2.2/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]


# Internationalization
# https://docs.djangoproject.com/en/2.2/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/2.2/howto/static-files/

STATIC_URL = "/staticfiles/"
STATIC_ROOT = os.path.join(BASE_DIR, "staticfiles")

MASTER_SESSION_TOKEN = os.environ.get('MASTER_SESSION_TOKEN')

LOCAL_MODE = True

AWS_ACCESS_KEY_ID = os.environ.get('AWS_ACCESS_KEY_ID')
AWS_SECRET_ACCESS_KEY = os.environ.get('AWS_SECRET_ACCESS_KEY')
AWS_REGION = os.environ.get('AWS_REGION')
SENDGRID_API_KEY = os.environ.get('SENDGRID_API_KEY')


import sentry_sdk
from sentry_sdk.integrations.django import DjangoIntegration

sentry_sdk.init(
    dsn="https://2f0d01e8da954672a702a59f878e2847@o502058.ingest.sentry.io/5584005",
    integrations=[DjangoIntegration()],
    traces_sample_rate=0.1,

    # If you wish to associate users to errors (assuming you are using
    # django.contrib.auth) you may enable sending PII data.
    send_default_pii=True
)
