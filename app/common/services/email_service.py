from django.core.mail import send_mail
from django.conf import settings
import logging
logger = logging.getLogger(__name__)


def send_email(subject, email_body, recepient_list):
	sender = settings.EMAIL_HOST
	if settings.DEBUG:
		recepient_list = settings.DEBUG_RECIEPIENT_LIST
	send_mail(subject,
		email_body,
    	sender,
    	reciepient_list,
    	fail_silently=True)


"""Base Emailer Service."""
from django.core.mail import get_connection
from django.core.mail import send_mail
from django.core.mail import EmailMessage
from django.template import loader
from django.conf import settings
from common.services.s3_upload_service import S3UploadService

# TODO: use send_mass_mail to club together one or more mails


class Mailer(object):
    """A class to implement a generic emailer service."""

    COMMON_PATH = "common/mail/html/templates/"
    COMMON_HTML = {
        "HEADER": "header.html",
        "FOOTER": "footer.html",
    }

    def send_bulk_mail(self, subject, message, send_to, content_type_is_html=True):
        """Send bulk Mail."""
        connection = get_connection()
        connection.open()
        email = EmailMessage(
            subject=subject,
            body=message,
            from_email=settings.EMAIL_SENDER,
            bcc=send_to,
            connection=connection)
        if content_type_is_html:
            email.content_subtype = "html"
        email.send()

    def send_mail(self, subject, html_message, send_to=None,
                  text=None, s3_attachments=None,
                  attachments=None):
        """Send the mail."""
        if not text:
            text = html_message
        if not send_to:
            send_to = settings.EMAIL_SENDER
        sender = settings.EMAIL_SENDER
        # send_mail(subject, text, sender, [send_to], fail_silently=False,
        #           html_message=html_message)
        if settings.SEND_EMAIL:
            email = EmailMessage(
                subject=subject,
                body=text,
                from_email=sender,
                to=[send_to])
            email.content_subtype = "html"
            if attachments:
                for attachment in attachments:
                    email.attach_file(attachment)
            if s3_attachments:
                # process keys here to get file pointer
                attachments = S3UploadService(
                    bucket_name='retool-data').\
                    get_s3_mail_attachments(s3_attachments)
                for attachment in attachments:
                    email.attach(**attachment)
            email.send()

    def send_referral_email(self, subject, html_message, send_to, sender, text=None):
        """Send the mail."""
        if not text:
            text = html_message
        if not send_to:
            send_to = self.ADMIN_EMAIL_ID
        send_mail(subject, text, sender, [send_to], fail_silently=False,
                  html_message=html_message)

    def get_html(self, filename=None, path=None, tag=None, **kwargs):
        """
        Get the html by path or tag.

        Return the HTML text from the path with the kwargs to fill
        in the data or if the path is None get the path from the tag
        key in self.COMMON_HTML.
        """
        if filename and hasattr(self, "custom_template_path"):
            path = "{path}{filename}".format(
                path=self.custom_template_path,
                filename=filename)
        if path:
            return loader.render_to_string(path, kwargs)
        if tag:
            return loader.render_to_string("".join([
                self.COMMON_PATH,
                self.COMMON_HTML[tag]
            ]), kwargs)
        return ""
