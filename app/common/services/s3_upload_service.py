"""S3 Upload Service."""
import boto3
import time
import base64
# import botocore
from botocore.client import ClientError
# import requests
from io import StringIO
from urllib.request import urlopen
from django.conf import settings
import re
import io
from PIL import Image
from common.exceptions import NotAcceptableError


class S3UploadService(object):
    """A service to upload things to S3."""

    BASE_PATH = "https://s3.ap-south-1.amazonaws.com/"
    CLOUDFRONT_BASE_PATH = "https://dz5r6tnhr6b2c.cloudfront.net/"

    def __init__(self, bucket_name='lqcdndata'):
        """Specify the bucket name."""
        self.bucket_name = bucket_name

        # check if the bucket exists
        s3 = self._get_s3_resource()
        try:
            s3.meta.client.head_bucket(Bucket=self.bucket_name)
        except ClientError:
            # The bucket does not exist or you have no access.
            s3.create_bucket(Bucket=self.bucket_name,
                             ACL='public-read',
                             CreateBucketConfiguration={
                                 'LocationConstraint': 'ap-south-1'
                             })

    def check_if_key_exists(self, key):
        """Check if the given key is already in the s3 bucket."""
        s3 = self._get_s3()
        try:
            s3.get_object_acl(
                Bucket=self.bucket_name,
                Key=key)
        except:  # NOQA
            return False
        return True

    def _get_s3(self):
        return boto3.client(
            's3',
            aws_access_key_id=settings.AWS_ACCESS_KEY_ID,
            aws_secret_access_key=settings.AWS_SECRET_ACCESS_KEY)

    def get_s3_mail_attachments(self, attachment_keys_list):
        attachments = []
        for path in attachment_keys_list:
            attachment_dict = dict()
            s3_url = self.BASE_PATH + self.bucket_name + '/' + path
            fp = urlopen(s3_url)
            attachment_dict['mimetype'] = fp.info().type
            attachment_dict['filename'] = path.split('/')[-1]
            attachment_dict['content'] = fp.read()
            attachments.append(attachment_dict)
        return attachments

    def _get_s3_resource(self):
        return boto3.resource(
            's3',
            aws_access_key_id=settings.AWS_ACCESS_KEY_ID,
            aws_secret_access_key=settings.AWS_SECRET_ACCESS_KEY)

    def upload_file_to_s3(self, filename, key=None, extension='jpg'):
        """Upload PDF the file to s3."""
        if not key:
            key = filename
        file_object = urllib2.urlopen(filename)  # 'Like' a file object
        fp = StringIO.StringIO(file_object.read())   # Wrap object
        return self.upload(
            data=fp,
            key=key,
            content_type="image/" + extension
        )

    def upload_base64_image_to_s3(self, data, name, compress_to=[256, 256]):
        """Upload a Base 64 image to s3."""
        self.delete(prefix=name + '_')
        if not data:
            # delete the image
            return None

        # Get the content type
        content_type = None
        r = re.findall(r'data:image/(.+);base64,(.+)', data)
        # print r
        image_data = None
        extension = None
        if r and isinstance(r, list):
            extension = r[0][0]
            content_type = "image/" + extension
            image_data = r[0][1]
        else:
            raise NotAcceptableError("The base64 image data is invalid")

        # Compress the image
        if compress_to:
            image_object = Image.open(io.BytesIO(base64.b64decode(image_data)))
            image_object.thumbnail(compress_to, Image.ANTIALIAS)
            image_data = io.BytesIO()
            image_object.save(image_data, format=extension.upper())
            image_data = image_data.getvalue()

        # print content_type
        # print r[0][1]
        return self.upload(
            data=image_data,
            key=name + "_" + str(int(time.time())) + "." + extension,
            content_type=content_type)

    def delete(self, key=None, prefix=None):
        """
        Delete a key from s3.

        Delete a object with the specified key or
        delete all objects with the given prefix.
        """
        s3 = self._get_s3_resource()
        if key:
            s3.delete_object(self.bucket_name, key)
        if prefix:
            objects_to_delete = s3.meta.client.list_objects(
                Bucket=self.bucket_name,
                Prefix=prefix)
            delete_keys = {'Objects': []}
            delete_keys['Objects'] = [
                {'Key': k} for k
                in [
                    obj['Key'] for obj in
                    objects_to_delete.get('Contents', [])
                ]
            ]
            if delete_keys['Objects']:
                s3.meta.client.delete_objects(Bucket=self.bucket_name,
                                              Delete=delete_keys)

    def upload(self, data, key, content_type='text/plain'):
        """Upload PDF the file to s3."""
        s3 = self._get_s3()
        s3.upload_fileobj(StringIO.StringIO(data),
                          self.bucket_name,
                          key,
                          ExtraArgs={
                              'ACL': 'public-read',
                              'ContentType': content_type
        })
        return self._get_url(key)

    # def delete_key(self, key):
    #     response = bucket.delete_objects(
    #         Delete={
    #             'Objects': [
    #                 {
    #                     'Key': 'myObjectKey'
    #                 }
    #             ]
    #         }
    #     )
#     def is_file_there(self, key):
#         """Check if the key exists in the Bucket."""
#         s3 = boto3.resource(
#             's3',
#             aws_access_key_id=AWS_ACCESS_KEY_ID,
#             aws_secret_access_key=AWS_SECRET_ACCESS_KEY)
#         try:
#             s3.Object(self.bucket_name, key).load()
#         except botocore.exceptions.ClientError:
#             # TODO: Ideally this should be false only if the status 404
#             return False, None
#         else:
#             return True, self._get_url(key)

    def _get_url(self, key):
        return "{base_path}{key}".format(
            base_path=self._get_base_path(),
            key=key)

    def _get_base_path(self):
        if self.bucket_name == "lqcdndata":
            return self.CLOUDFRONT_BASE_PATH
        return self.BASE_PATH + self.bucket_name

    # def _get_cloudfront_url(self, key):
    #     cloudfront_path = "https://dz5r6tnhr6b2c.cloudfront.net/"
    #     return "{cloudfront_path}{key}".format(
    #         cloudfront_path=cloudfront_path,
    #         key=key)
