from django.apps import apps
from django.contrib import admin
from .models import *
from django.db.models import ManyToOneRel, ForeignKey, OneToOneField , ManyToManyRel,ManyToManyField,AutoField

excluding_list_display = ('created_at','updated_at','is_active','deleted_at')
excluding_fields = ('created_at','updated_at','is_active','deleted_at')

models = apps.get_app_config('user').get_models()

for model in models:
    try:
        class CustomModelAdmin(admin.ModelAdmin):
            fields = [field.name for field in model._meta.get_fields() if not isinstance(field, (ManyToOneRel,ManyToManyRel,AutoField)) and field.name not in excluding_fields ]
            list_display = [field.name for field in model._meta.get_fields() if not isinstance(field, (ManyToOneRel,ManyToManyRel,ManyToManyField,AutoField)) and field.name not in excluding_list_display ]

        admin.site.register(model,CustomModelAdmin)
    except admin.sites.AlreadyRegistered:
        pass