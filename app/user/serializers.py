"""Serializers for the User App."""
import uuid
from rest_framework import serializers
from common.exceptions import NotAcceptableError
from rest_framework.validators import UniqueValidator
from common.utils import encryption_utils
from django.utils import timezone
from datetime import timedelta
from django.conf import settings
